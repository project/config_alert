#CONTENTS OF THIS FILE
   
## INTRODUCTION
 * This module dispatches an event when configuration is saved.

## REQUIREMENTS
 * No module required.

## INSTALLATION
 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/documentation/install/modules-themes/modules-7
   for further information.
   
## MAINTAINERS
Current maintainers:
 * Thomas Sécher - https://www.drupal.org/u/tsecher