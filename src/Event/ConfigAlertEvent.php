<?php

namespace Drupal\config_alert\Event;

use Drupal\config_alert\Config\ConfigDecorator;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ConfigAlertEvent.
 *
 * @package Drupal\config_alert\Event
 */
class ConfigAlertEvent extends Event {

  /**
   * Event Name.
   *
   * @const string
   */
  const ON_CHANGE = 'config_alert_event__on_change';

  /**
   * Config modifiée.
   *
   * @var \Drupal\config_alert\Config\ConfigDecorator
   */
  protected $config;

  /**
   * ConfigAlertEvent constructor.
   */
  public function __construct(ConfigDecorator $config) {
    $this->config = $config;
  }

  /**
   * Retourne la configuration.
   *
   * @return \Drupal\config_alert\Config\ConfigDecorator
   *   La configuration.
   */
  public function getConfig() {
    return $this->config;
  }

}
