<?php

namespace Drupal\config_alert\Service;

use Drupal\Core\Config\ConfigFactory;
use Drupal\config_alert\Config\ConfigDecorator;

/**
 * Class AlertConfigDecorator.
 *
 * Redéfinition de la config Factory pour renvoyer un objet mutable de type
 * ConfigDecorator.
 *
 * @package Drupal\config_alert\Service
 */
class ConfigAlertDecorator extends ConfigFactory {

  /**
   * {@inheritdoc}
   */
  protected function createConfigObject($name, $immutable) {
    if ($immutable) {
      return parent::createConfigObject($name, $immutable);
    }

    return new ConfigDecorator($name, $this->storage, $this->eventDispatcher, $this->typedConfigManager);
  }

}
