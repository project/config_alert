<?php

namespace Drupal\config_alert;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Class ConfigAlertServiceProvider.
 *
 * Redéfinition du service config.factory.
 *
 * @package Drupal\config_alert
 */
class ConfigAlertServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    parent::alter($container);

    $definition = $container->getDefinition('config.factory');
    $definition->setClass('Drupal\config_alert\Service\ConfigAlertDecorator');
  }

}
