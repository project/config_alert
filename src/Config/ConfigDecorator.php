<?php

namespace Drupal\config_alert\Config;

use Drupal\config_alert\Event\ConfigAlertEvent;
use Drupal\Core\Config\Config;

/**
 * Class ConfigDecorator.
 *
 * Dispatch un événement lors d'une modification de conf.
 *
 * @package Drupal\config_alert\Config
 */
class ConfigDecorator extends Config {

  /**
   * {@inheritdoc}
   */
  public function save($has_trusted_data = FALSE) {
    $result = parent::save($has_trusted_data);

    /** @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $dispatcher */
    $dispatcher = \Drupal::service('event_dispatcher');
    $dispatcher->dispatch(ConfigAlertEvent::ON_CHANGE, new ConfigAlertEvent($this));

    return $result;
  }

}
